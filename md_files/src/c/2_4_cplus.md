```
class FullName
{
public FullName(string firstName, string lastName)
{
FirstName = firstName;
LastName = lastName;
}
public string FirstName { get; }
public string LastName { get; }
}
```

```
var fullName = new FullName("masanobu", "naruse");
Console.WriteLine(fullName.LastName); // naruse が表示される
```