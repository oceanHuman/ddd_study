```
public class Client{
  private UserApplicationService userApplicationService;
  （…略…）
  public void ChangeName(string id, string name){
    var target = userApplicationService.Get(id);
    var newName = new UserName(name);
    target.ChangeName(newName);
  }
}
```