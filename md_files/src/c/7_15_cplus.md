```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  // 新たにIFooRepositoryへの依存関係を追加する
  private readonly IFooRepository fooRepository;

  // コンストラクタで依存を注入できるようにする
  public UserApplicationService(IUserRepositoryuserRepository, IFooRepository fooRepository){
    this.userRepository = userRepository;
    this.fooRepository = fooRepository;
  }
  （…略…）
}
```