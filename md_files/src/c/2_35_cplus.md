```
class ModelNumber
{
  public ModelNumber(string productCode, string branch,string lot){
    this.productCode = productCode;
    this.branch = branch;
    this.lot = lot;
  }

  public override string ToString(){
    return productCode + "-" + branch + "-" + lot;
  }
}
```