```
class FullName : IEquatable<FullName>
  {
  （…略…）
  public bool Equals(FullName other)
  {
    if (ReferenceEquals(null, other)) return false;
    if (ReferenceEquals(this, other)) return true;
    return string.Equals(firstName, other.firstName)
    && string.Equals(lastName, other.lastName)
    && string.Equals(middleName, other.middleName); // 条件式の追加はここだけ
  }
}

```