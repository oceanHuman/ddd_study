```
interface IUserRepository {
  void UpdateName(UserId id, UserName name);
  void UpdateEmail(UserId id, Email mail);
  void UpdateAddress(UserId id, Address address);
  （…略…）
}
```