```
class UserService {
  private IUserRepository userRepository;
  （…略…）
  public bool Exists(User user) {
    // ユーザ名により重複確認を行うという知識は失われている
    return userRepository.Exists(user);
  }
}
```