```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  public UserApplicationService() {
    // ServiceLocator経由でインスタンスを取得する
    this.userRepository = ServiceLocator.Resolve<IUserRepository>();
  }
  （…略…）
}
```