```
class User : IEquatable<User>
{
  private readonly UserId id;
  private string name;
  （…略…）
  public bool Equals(User other){
    return Equals(id, other.id); // 比較は id 同士で行われる
  }
}
```