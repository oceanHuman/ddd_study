```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  // 新たなフィールドが追加された
  private readonly IFooRepository fooRepository;

  public UserApplicationService(){
    this.userRepository =ServiceLocator.Resolve<IUserRepository>();
    // ServiceLocator経由で取得している
    this.fooRepository =ServiceLocator.Resolve<IFooRepository>();
  }
  （…略…）
}
```