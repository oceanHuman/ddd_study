```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  （…略…）
  public User Get(string userId){
    var targetId = new UserId(userId);
    var user = userRepository.Find(targetId);
    return user;
  }
}
```