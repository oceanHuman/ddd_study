```
var greet = "こんにちは";
greet.ChangeTo("Hello"); // このようなメソッドは本来存在しない
Console.WriteLine(greet); // Hello が表示される
```
```
"こんにちは".ChangeTo("Hello"); // このようなメソッドは本来存在しない
Console.WriteLine("こんにちは"); // Hello が表示される
```
