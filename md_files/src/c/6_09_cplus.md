```
public class UserApplicationService {
  private readonly IUserRepository userRepository;
  （…略…）
  public UserData Get(string userId){
    var targetId = new UserId(userId);
    var user = userRepository.Find(targetId);
    var userData = new UserData(user.Id.Value,
    user.Name.Value);
    return userData;
  }
}
```