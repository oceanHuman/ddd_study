```
interface IUserRepository {
  User Find(UserName name);
  // オーバーロードがサポートされていない言語の場合は命名によりバリエーションを増やす
  User FindByUserName(UserName name);
  （…略…）
}
```