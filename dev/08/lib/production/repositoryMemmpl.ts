import { injectable } from "inversify";
import { IRepository } from "../interfaces/IRepository";

@injectable()
export class RepositoryImpl implements IRepository {
    public add(): void {
        console.log("RepositoryImpl.func()");
    }
}