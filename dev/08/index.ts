import { Container } from "inversify";

import "reflect-metadata";

import { IRepository } from "./lib/interfaces/IRepository";
import { RepositoryImpl } from "./lib/production/repositoryMemmpl";
import  prompts from "prompts";

function startup(): Container {
  const container = new Container();
  container.bind<IRepository>("Repository").to(RepositoryImpl);
  return container;
}

async function main(): Promise<number> {
  const container:Container = startup();
  const response = await prompts({
    type: 'text',
    name: 'value',
    message: 'Input user name',
    validate: (value:any) => value < 18 ? `Nightclub is 18+ only` : true
  });
  console.log(response);
  const repo = container.get<IRepository>("Repository");
  repo.add()
  return 0;
}

main();
