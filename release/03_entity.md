---
marp: true

---
<!-- 
theme: default
size: 16:9
paginate: true
style: |
  section {
    background-color: #FFFFFF;
  }
-->

<!-- header: ドメイン駆動設計:値オブジェクト -->
<!-- headingDivider: 2 -->
<!-- auto-scaling true -->


# エンティティとは

ドメイン駆動設計におけるエンティティは、ドメインモデルを実装したドメインオブジェクトです。
値オブジェクトもドメインモデルを実装したドメインオブジェクトです。
両者の違いは同一性によって識別されるか否かです。

## エンティティの性質について

エンティティは属性ではなく同一性によって識別されるオブジェクトです。
値オブジェクトの性質を真逆にしたような性質もあります。

1. 可変である
1. 同じ属性であっても区別される
1. 同一性により区別される

## 可変である

値オブジェクトは不変なオブジェクトでした。
それに比べてエンティティは可変なオブジェクトです。
人々がもつ年齢や身長といった属性が変化するのと同じように、エンティティの属性は変化することが許容されています。

## 可変ではないユーザーを表すクラス

```
class User
{
  private string name;
  public User(string name){
    if (name == null) throw new ArgumentNullException(nameof(name));
    if (name.Length < 3) throw new ArgumentException("ユーザ名は３文字以上です。", nameof(name));

    this.name = name;
  }
}
```


## 可変なユーザーを表すクラス

```
class User
{
  private string name;
  public User(string name){
    ChangeName(name);
  }

  public void ChangeName(string name){
    if (name == null) throw new ArgumentNullException(nameof(name));
    if (name.Length < 3) throw new ArgumentException("ユーザ名は３文字以上です。", nameof(name));

    this.name = name;
  }
}
```
値オブジェクトは不変の性質が存在するため交換（代入）によって変更を表現していましたが、エンティティは交換により変更を行いません。
エンティティの属性を変化させたいときには、そのふるまいを通じて属性を変更することになります
可変なオブジェクトは基本的には厄介な存在です。可能な限り不変にしておくことはよい習慣です。

## 同じ属性であっても区別される
値オブジェクトは同じ属性であれば同じものとして扱われました。
エンティティはそれと異なり、たとえ同じ属性であっても区別されます。
エンティティは人間として例えることができます。
氏名が一致したからといって必ずしも同じ人物のことを指していると断定はできません。
エンティティを区別するためには識別子（Identity）が利用されます。

## 識別子とそれを利用したユーザのオブジェクト

```
class UserId
{
  private string value;
  public UserId(string value){
    if (value == null) throw new ArgumentNullException(nameof(value));

    this.value = value;
  }
}

class User
{
  private readonly UserId id;
  private string name;
  public User(UserId id, string name){
    if (id == null) throw new ArgumentNullException(nameof(id));
    if (name == null) throw new ArgumentNullException(nameof(name));

    this.id = id;
    this.name = name;
  }
}
```

## 同一性をもつ

エンティティは可変なオブジェクトです。
可変なオブジェクトですが、その属性が変更しても変更前と同じオブジェクトとして扱われます。

```
class User
{
  private readonly UserId id; // 識別子
  private string name;
  public User(UserId id, string name){
    if (id == null) throw new ArgumentNullException(nameof(id));
    this.id = id;
    ChangeUserName(name);
  }
  public void ChangeUserName(string name){
    if (name == null) throw new ArgumentNullException(nameof(name));
    if (name.Length < 3) throw new ArgumentException("ユーザ名は３文字以上です。", nameof(name));

      this.name = name;
    }
}
```

## 同一性を比較するメソッドを提供する

```
class User : IEquatable<User>
{
  private readonly UserId id;
  private string name;
  （…略…）
  public bool Equals(User other){
    return Equals(id, other.id); // 比較は id 同士で行われる
  }
}
```

値オブジェクトの比較処理ではすべての属性が比較の対象となっていましたが、エンティティの比較処理では同一性を表す識別子（id）だけが比較の対象となります。これにより、エンティティは属性の違いにとらわれることなく同一性の比較が可能になります

## 同一性を比較するメソッドを利用する

```
void Check(User leftUser, User rightUser){
  if (leftUser.Equals(rightUser)){
    Console.WriteLine("同一のユーザです");
  }else{
    Console.WriteLine("別のユーザです");
  }
}
```



## エンティティの判断基準としてのライフサイクルと連続性

値オブジェクトとエンティティはドメインの概念を表現するオブジェクトとして似通っています。
何を値オブジェクトにして、何をエンティティにするか。
そこにライフサイクルが存在し、そこに連続性が存在するかというのは大きな判断基準になります。

もしもライフサイクルをもたない、またはシステムにとってライフサイクルを表現することが無意味である場合には、ひとまずは値オブジェクトとして取り扱うとよいでしょう。
不変にしておけるものは可能な限り不変なオブジェクトのままにして取り扱うことは、シンプルなソフトウェアシステムを構築する上で大切なことです

## ドメインオブジェクトを定義するメリット

1. コードのドキュメント性が高まる
1. ドメインにおける変更をコードに伝えやすくする

## コードのドキュメント性が高まる

値オブジェクトと似ているメリットになります

```
class User
{
  public string Name { get; set; }
}
```

無口なコード

## ドキュメント性が高いコード

```
class UserName
{
  private readonly string value;
  public UserName(string value){
    if (value == null) throw new ArgumentNullException(nameof(value));
    if (value.Length < 3) throw new ArgumentException("ユーザ名は３文字以上です。", nameof(value));

    this.value = value;
  }
  （…略…）
}
```

UserNameクラスを見たときに、ユーザ名は3文字以上でないと動作しないことは自明です。
コードを饒舌にする努力を怠らなければ、開発者はコードを手掛かりにして、そこに存在するルールを確認できるのです。

## ドメインにおける変更をコードに伝えやすくする
<style>p {font-size: 93%; } </style>
ドメインオブジェクトにふるまいやルールを記述することは、ドメインにおける変更をコードに伝えやすくする効果があります。
例えば「ユーザ名は最小3文字」というルールが「ユーザ名は最小6文字」に変更されたときです。
このルールの変化はコードにも反映する必要があります。
そのときユーザを表すUserクラスのコードがリスト3.8のようなただのデータ構造体であったならば、その変更は極めて困難な道のりです。
プログラムの随所に散らばったコードから変更すべき箇所をを探し出す必要があります。
反対に、もしコードがリスト3.9のようにそこにあるルールを語っていたらどうでしょうか。
ドメインモデルのルールが記載されているところは明白で、その修正もまた容易いものでしょう。

## まとめ
本章では値オブジェクトと並んで重要なモデルを表現するオブジェクトであるエンティティについて解説をしました。
豊かなふるまいをもったオブジェクトはソフトウェアがどのドメインの知識に関心があるか、それをどのように識別しているかということを浮き彫りにします。これはもちろん後続の開発者にとってドメインを理解する有効な手掛かりになります。
ドメインに対する鋭い洞察は実装時にも現れます。これは人が得意とする曖昧さをソフトウェアが受け入れられないことに端を発します。もしもエンティティを実装しようとしてそこに曖昧さを感じたのであれば、それはドメインの捉え方を見つめ直すきっかけです。




