---
marp: true

---
<!-- 
theme: default
size: 16:9
paginate: true
style: |
  section {
    background-color: #FFFFFF;
  }
-->

<!-- header: ドメイン駆動設計:値オブジェクト -->
<!-- headingDivider: 2 -->

# 値オブジェクトとは
このシステム固有の値を表現するために定義されたオブジェクトが値オブジ ェクトと呼ばれるものです。

そもそもなぜそれが必要になるのか
一度値オブジェクト無しでプリミティブな値だけで組んだコードを見ます

## プリミティブで表現した場合

### プリミティブな値で「氏名」を表現する
```
var fullName = "naruse masanobu";
Console.WriteLine(fullName); // naruse masanobu が表示される
```

### 姓だけを表示する
```
var fullName = "naruse masanobu";
var tokens = fullName.Split(' '); // ["naruse", "masanobu"] という配列に
var lastName = tokens[0];
Console.WriteLine(lastName); // naruse が表示される
```

## うまく姓を表示できないパターン
名姓という組み合わせで名前を表現する世界ではこのコードは通用せず、名が表示されてしまう
```
var fullName = "john smith";
var tokens = fullName.Split(' '); // ["john", "smith"] という 配列に
var lastName = tokens[0];
Console.WriteLine(lastName); // john が表示される
```


## クラスを利用した場合
FullNameクラスはコンストラクタで第１引数に名、第2引数に姓を 指定するようになっている
そのルールさえ守られれば、たとえ姓と名の順序が入れ替わるような氏名であっても姓を取り出すことが可能
```
class FullName
{
public FullName(string firstName, string lastName)
{
FirstName = firstName;
LastName = lastName;
}
public string FirstName { get; }
public string LastName { get; }
}
```

```
var fullName = new FullName("masanobu", "naruse");
Console.WriteLine(fullName.LastName); // naruse が表示される
```

## 姓名が逆転した場合クラスを利用した場合
```
var fullname = new FullName("john", "smith");
Console.WriteLine(fullname.LastName); // smith が表示される
```

## 値オブジェクトについて
このFullNameはその名のとおり氏名を表現したオブジェクトで、値の表現なのです。
この例からわかることは、**システムに最適な値**が必ずしもプリミティブな値であるとは限らないということです。
システムには必要とされる処理にしたがって、そのシステムならではの値の表現があるはずです。
FullNameクラスはまさにそういった値の表現です。
オブジェクトであり、値でもある。ゆえに値オブジェクトである。
ドメイン駆動設計ではこのようにシステム固有の値を表したオブジェクトを値オブジェクトと呼んでいます。

# 値オブジェクトの性質
値オブジェクトは値でもあることから値オブジェクトは値の性質を持ちます。
値は大きく3つの性質を持ちます

1. 不変
1. 交換が可能
1. 等価性によって比較される

## 不変
### 値の変更をしている?
```
var greet = "こんにちは";
Console.WriteLine(greet); // こんにちは が表示される
greet = "Hello";
Console.WriteLine(greet); // Hello が表示される
```

値が不変っていうのは奇妙な響きです。
プログラムを組んでいるときに値を操作しているはずです。
でも厳密にいうと変数の内容を変更しているだけで、値そのものを変えているわけではありません

## もし値の変更をできたなら
```
var greet = "こんにちは";
greet.ChangeTo("Hello"); // このようなメソッドは本来存在しない
Console.WriteLine(greet); // Hello が表示される
```
```
"こんにちは".ChangeTo("Hello"); // このようなメソッドは本来存在しない
Console.WriteLine("こんにちは"); // Hello が表示される
```

値が変更できてしまうとバグのもとになるのは大いに予想できます。
今日の自分がバグを埋め込み、明日の自分が泣くのです

## 不変のメリット1
ときにソフトウェア開発はバグとの闘いになることもあります。
バグはさまざまなことを起因として開発者を悩ませますが、状態の変化もその原因の1つです。
生成したインスタンスをメソッドに引き渡したら、自身のあずかり知らぬところでいつの間にか状態の変更をされ、
意図せぬ挙動となりバグを引き起こしてしまった、といった失敗話は非常にありふれています。
状態の変更を起因とするバグを防ぐために取れるもっとも単純な作戦は、状態を不変にすることです。
いつの間にか変更されることが問題であるなら、そもそも変更できないようにしてしまえばよいのです。
これは単純ながら強力な防衛策です。
## 不変のメリット2
状態を変更できないというのはプログラムをシンプルにする可能性を秘めた制約です。
たとえば並行・並列処理を伴うプログラムでは、変化する可能性のあるオブジェクトに対するアプローチは課題になります。
変化をしないオブジェクトであれば、値の変更を考慮する必要がなくなり、並行・並列処理を比較的容易に実装できます。
また他にも、たとえばコンピュータのリソースであるメモリがひっ迫したときに、オブジェクトをキャッシュする戦略を取ることができます。
状態が変更されないオブジェクトであれば、まったく同じ状態のオブジェクトを複数準備する必要はありません。
ひとつのオブジェクトをキャッシュして使いまわすことでリソースの節約が可能です。

## 不変のメリット3
もちろんオブジェクトを不変にすることによるデメリットも存在します。
代表的なデメリットとして挙げられるのは、オブジェクトの値を一部変更したいときに、新たなインスタンスを生成する必要があるということです。
これは状態を変更できるときに比べてパフォーマンスの観点で不利になります。
あまりに深刻な事態となる場合には値オブジェクトであっても可変を許容するという戦略をとることすらあるでしょう。
とはいえ、可変なオブジェクトを不変なオブジェクトに変更するのと不変なオブジェクトを可変なオブジェクトに変更するのでは、後者の方が労力は少ないです。
もしも可変にするか不変にするかに迷うようなことがあれば、いったんは不変なオブジェクトにしておくとよいでしょう。


# 交換が可能

値は不変です。
しかし、プログラムを作るにあたって値を変化せずに目的を達成するのは難しい。
値は不変ですが値を交換(変更)することは必要となります。
これは不変であることと矛盾しているように感じます
具体的にどういうことか実際のコードで確認していきましょう

## 普段行っている値の変更

```
// 数字の変更
var num = 0;
num = 1;
// 文字の変更
var c = '0';
c = 'b';
// 文字列の変更
var greet = "こんにちは";
greet = "hello";
```

いずれも代入をしています。これは代入こそが値の
変更の表現方法であるということです

「不変」の性質をもつ値はそれ自体を変更できません。値オブジェクトにおいてもこれは同じことです。
値オブジェクトの変更は値と同じように代入操作によって交換をすることで表現されます。

## 値オブジェクトの変更方法

```
var fullName = new FullName("masanobu", "naruse");
fullName = new FullName("masanobu", "sato");
```

記述の仕方は、値の変更（代入）と同じ記述です
値オブジェクトが不変であるがゆえに、代入操作による交換以外の手段で変更を表現できなくなっています。

## 等価性によって比較される

同じ種類の値同士は以下のように比較することができます。

```
Console.WriteLine(0 == 0); // true
Console.WriteLine(0 == 1); // false
Console.WriteLine('a' == 'a'); // true
Console.WriteLine('a' == 'b'); // false
Console.WriteLine("hello" == "hello"); // true
Console.WriteLine("hello" == "こんにちは"); // false
```

たとえば0 == 0という式の左辺の0と右辺の0はインスタンスとして別個のものですが、等価として扱われます。
値は値自身でなく、それを構成する属性によって比較されるということです。

## 値オブジェクト同士の比較

システム固有の値である値オブジェクトも値と同様に、値オブジェクトを構成する属性によって比較されます

```
var nameA = new FullName("masanobu", "naruse");
var nameB = new FullName("masanobu", "naruse");
// 別個のインスタンス同士の比較
Console.WriteLine(nameA.Equals(nameB)); // インスタンスを構成する属性が等価なのでtrue
```


## 属性を取り出して行う不適切な比較(値オブジェクト)

```
var nameA = new FullName("masanobu", "naruse");
var nameB = new FullName("john", "smith");
var compareResult =
  nameA.FirstName == nameB.FirstName
  && nameA.LastName == nameB.LastName;
Console.WriteLine(compareResult);
```

実行可能なモジュールであるという意味でこのコードは正しく、一見すると自然なコードに見えます。しかし、FullNameオブジェクトが値であることを考えると不自然な記述です。

## 属性を取り出して行う不適切な比較(プリミティブ)

```
Console.WriteLine(1.Value == 0.Value); // false ?
```

値の値（Value）を取り出すとはいったいどういうことなのでしょうか。これはいかにも不自然な記述です。
値オブジェクトはシステム固有の値です。あくまでも値なのです。その属性を取り出して比較をするのではなく、値と同じように値オブジェクト同士が比較できるようにする方が自然な記述になります

## 値同士で比較する

```
var nameA = new FullName("masanobu", "naruse");
var nameB = new FullName("john", "smith");
var compareResult = nameA.Equals(nameB);
Console.WriteLine(compareResult);
// 演算子のオーバーライド機能を活用することも選択肢に入る
var compareResult2 = nameA == nameB;
Console.WriteLine(compareResult2);
```


このような自然な記述を行うためには値オブジェクトが比較するためのメソッドを提供する必要があります

## 比較メソッドを提供するFullNameクラス
```
class FullName : IEquatable<FullName>
{
  public FullName(string firstName, string lastName)
  {
    FirstName = firstName;
    LastName = lastName;
  }
  public string FirstName { get; }
  public string LastName { get; }
  public bool Equals(FullName other)
  {
    if (ReferenceEquals(null, other)) return false;
    if (ReferenceEquals(this, other)) return true;
    return string.Equals(FirstName, other.FirstName)
    && string.Equals(LastName, other.LastName);
  }
  public override bool Equals(object obj)
  {
    if (ReferenceEquals(null, obj)) return false;
    if (ReferenceEquals(this, obj)) return true;
    if (obj.GetType() != this.GetType()) return false;
    return Equals((FullName) obj);
  }
  // C#ではEqualsをoverrideする際にGetHashCodeをoverrideするルールがある
  public override int GetHashCode()
  {
    unchecked
    {
      return ((FirstName != null ? FirstName.GetHashCode() :0) * 397)
      ^ (LastName != null ? LastName.GetHashCode() : 0);
    }
  }
}
```

## 解説
このコードはC#における典型的な比較の実装方法です。値オブジェクト同士を比較する際には値オブジェクトの属性を取り出して比較するのではなく、Equalsメソッドを利用して比較を行います。これにより値オブイジェクトは値と同じように比較できるようになります。

## 値オブジェクトが比較メソッドを提供するメリット

この手法は記述として「自然であるということ以外に明確なメリットがあります。
そのメリットは値オブジェクトに新たなインスタンス変数が追加されたときにわかります。

## 新たに追加された属性も比較するように修正する

```
var compareResult = 
  nameA.FirstName == nameB.FirstName
  && nameA.LastName == nameB.LastName
  && nameA.MiddleName == nameB.MiddleName; //条件式を追加

```

この改修の難易度は易しいものです。
しかしながら、果たしてFullNameの比較を行っているのはここだけにとどまるでしょうか。
ひょっとするとFullNameの比較はここ以外でも行われていて、上記と同じような記述がプログラムの至る所に記述されてしまってはいないでしょうか。
修正した箇所の分岐の中と外のテストしましょう😂😂

## 新たな属性が追加されたときの修正(比較メソッド)

```
class FullName : IEquatable<FullName>
  {
  （…略…）
  public bool Equals(FullName other)
  {
    if (ReferenceEquals(null, other)) return false;
    if (ReferenceEquals(this, other)) return true;
    return string.Equals(firstName, other.firstName)
    && string.Equals(lastName, other.lastName)
    && string.Equals(middleName, other.middleName); // 条件式の追加はここだけ
  }
}

```

FullNameのインスタンスを比較するとき、開発者はこのEqualsメソッドを呼び出して比較を行います。
したがって、値オブジェクトに新たに属性が追加されたとしても、その変更はEqualsメソッドの修正だけで済みます。
比較処理に限らず、値の属性を操作する処理を値オブジェクトが提供してまとめることは変更箇所をまとめることと同義です。(DRYや集約の概念)

## 値オブジェクトにする基準

ドメインモデルとして挙げられていなかった概念を値オブジェクトにすべきかどうかの判断基準として、筆者は「そこにルールが存在しているか」という点と「それ単体で取り扱いたいか」という点を重要視しています。

## プリミティブ vs 値オブジェクトvs ネスト値オブジェクト


```
var p_fullName = "miyagi kaito";

var v_fullName = new FullName("miyagi","kaito");

var first_name = new FirstName("kaito");
var last_name  = new LastName("kaito");
var n_fullName = new FullName(first_name,last_name);
```
ルールが存在している部分はどこか見極めよう
単体で取り扱いたい値を設計しよう

## ふるまいをもった値オブジェクト

値オブジェクトで重要なことは独自のふるまいを定義できることです。

## ベースの値オブジェクト
```
class Money
{
  private readonly decimal amount;
  private readonly string currency;

  public Money(decimal amount, string currency){
    if (currency == null) throw new
    ArgumentNullException(nameof(currency));
    this.amount = amount;
    this.currency = currency;
  }
}
```

お金を表現する場合

## ベースの値オブジェクトに独自のふるまいを定義する
```
class Money
{
  private readonly decimal amount;
  private readonly string currency;
  （…略…）
  public Money Add(Money arg) {
    if (arg == null) throw new ArgumentNullException(nameof(arg));
    if (currency != arg.currency) throw new ArgumentException($"通貨単位が異なります（this:{currency}, arg:{arg.currency}）");

    return new Money(amount + arg.amount, currency);
  }
}
```


## ベースの値オブジェクトに独自のふるまいを定義した場合

```
var myMoney = new Money(1000, "JPY");
var allowance = new Money(3000, "JPY");
var result = myMoney.Add(allowance);
```
```
var jpy = new Money(1000, "JPY");
var usd = new Money(10, "USD");
var result = jpy.Add(usd); // 例外が送出される
```

値オブジェクトがふるまいについて定義しておけば、ロジックの散在を防げる

## 値オブジェクトを採用するモチベーション

概念はわかったけど、あんまり値オブジェクトを採用するモチベーションがわかない。。
という人もいるかと思います。
このセクションでは値オブジェクトを採用するメリットについて説明します

1. 表現力を増す
1. 不正な値を存在させない
1. 誤った代入を防ぐ
1. ロジックの散在を防ぐ

## 表現力を増す

プリミティブな値を利用した製品番号
```
var modelNumber = "a20421-100-1";
```

## 表現力を増す
```
class ModelNumber
{
  public ModelNumber(string productCode, string branch,string lot){
    this.productCode = productCode;
    this.branch = branch;
    this.lot = lot;
  }

  public override string ToString(){
    return productCode + "-" + branch + "-" + lot;
  }
}
```
ModelNumberクラスの定義を確認してみれば、製品番号はプロダクトコード（productCode）と枝番（branch）、そしてロット番号（lot）から構成されていることがわかります。
これは無口な文字列と比べると大きな進歩です。
値オブジェクトはその定義により自分がどういったものであるかを主張する自己文書化を推し進めます。

## 不正な値を存在させない
「ユーザ名は3文字以上」というルールが要件が存在するとき、プリミティブで実装されたコードは誤った値の存在を許してしまう。
```
var userName = "me";
```
値オブジェクトはそれを許さず、例外を出す
誰が実装しても(ココ重要)不正な値を許さない
```
class UserName
{
  private readonly string value;

  public UserName(string value){
    if (value == null) throw new ArgumentNullException(nameof(value));
    if (value.Length < 3) throw new ArgumentException("ユーザ名は3文字以上です。", nameof(value));

    this.value = value;
  }
}
```

## 誤った代入を防ぐ

誰にでも間違いはありますが、それをなるべく早い段階で発見したいですよね
実行したときに不整合が起きて、バグを発見するコード
```
User CreateUser(string name){
  var user = new User();
  user.Id = name;//idにnameを入れている
  return user;
}
```

## コンパイルエラーが起きて実行前に気付けるコード
```
class User
{
  public UserId Id { get; set; }
  public UserName Name { get; set; }
}
```
```
User CreateUser(UserName name)
{
  var user = new User();
  user.Id = name; // コンパイルエラー！
  return user;
}
```


## ロジックの散在を防ぐ
Equalメソッドを実装したときの副作用と同じ効果
メソッドを個別で実装するよりも、値オブジェクトなどにふるまいを持たせて
必ずそこを使う（通る）ようにすれば要件が変更になったときやロジックにバグがあり、修正しないといけないときも１か所の修正で済む

