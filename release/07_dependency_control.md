---
marp: true

---
<!-- 
theme: default
size: 16:9
paginate: true
style: |
  section {
    background-color: #FFFFFF;
  }
-->

<!-- header: ドメイン駆動設計 -->
<!-- headingDivider: 2 -->
<!-- auto-scaling true -->

# 柔軟性をもたらす依存関係のコントロール

## 技術要素への依存がもたらすもの1
![](./md_files/img/07/0701.PNG)


積み上がった積み木の中ほどに位置するブロックを抜き出すことを想像してみてください。
抜き出そうとしたブロックは直上のブロックを支えています。
手荒くブロックを抜き出せば、たちまちに積み木は崩れ去るでしょう。
誰しもがブロックを抜き出すことを難しいと感じるはずです。プログラムにおける依存も同じことがいえます。

## 技術要素への依存がもたらすもの2
<style scoped>p {font-size: 88%; } </style>
ソフトウェアの中核に位置するオブジェクトを変更することを想像してみてください。そのオブジェクトは多くのオブジェクトに依
存されていますし、多くのオブジェクトに依存しています。たったひとつの変更が多くのオブジェクトに影響します。
緻密に組み上げられたコードを変更することの厄介さは、ほとんど恐怖と似たような感覚となって開発者に襲い掛かるでしょう。
プログラムを組み上げていく過程でオブジェクト同士の依存は避けられません。
依存関係はオブジェクトを利用するだけで発生するのです。重要なことは依存を避けることではなく、コントロールすることです。
本章で解説する依存のコントロールはドメインのロジックを技術的要素から解き放ち、ソフトウェアに柔軟性を与えるものです。
コードが技術的要素に支配されることの問題を確認し、その解決法を確認していきましょう。

## 依存とは

```
public class ObjectA
{
  private ObjectB objectB;
}
```

ObjectAはObjectBを参照しています。そのため、ObjectBの定義が存在しない限りObjectAは成り立ちません。
ObjectAはObjectBに依存しているといえます。

## 依存とは

![](./md_files/img/07/0702.PNG)
こういった依存関係は図で表すことができます。
依存は依存する側のモジュールから依存される側のモジュールへと矢印を伸ばして表現します。

## 依存とは(UserApplicationServiceの例)

<style scoped>p {font-size: 90%; } </style>
```
public class UserApplicationService
{
  private readonly UserRepository userRepository;
  public UserApplicationService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }
  （…略…）
}
```

UserApplicationServiceにはUserRepositoryがフィールドとして定義されています。
したがってUserApplicationServiceはUserRepositoryに依存している状態です
このUserApplicationServiceには問題があります。
具体的にいえば具象クラスであるUserRepositoryクラス、ひいてはそこで利用されているデータ永続化に関する特定の技術基盤に依存していることが問題です。
UserRepositoryが取り扱っているデータストアに縛られ、 UserApplicationServiceがいずれかのデータストアに対して結びついています。
テストの実行やデータストアのチューニングを妨げています。

## 依存とは(UserApplicationServiceの例)

![](./md_files/img/07/0703.PNG)

UserApplicationServiceの依存関係を図に表す

## 依存関係逆転の原則とは
オブジェクト志向設計の概念の一つ
依存関係逆転の原則（Dependency Inversion Principle）は次のように定義されています

1. 上位レベルのモジュールは下位レベルのモジュールに依存してはならない、どちらのモジュールも抽象に依存すべきである。
1. 抽象は、実装の詳細に依存してはならない。実装の詳細が抽象に依存すべきである。


## 抽象に依存する1
プログラムにはレベルと呼ばれる概念があります。レベルは入出力からの距離を示します。
低レベルといえば機械に近い具体的な処理を指し、高レベルといえば人間に近い抽象的な処理を指します。
依存関係逆転の原則に表れる上位レベルや下位レベルというのはこれと同じです。

## 抽象に依存する2
たとえばデータストアを操作するUserRepositoryの処理は、UserRepositoryを操作するUserApplicationServiceよりも機械に近い処理です。
レベルの概念に照らし合わせるとUserRepositoryは下位レベルでUserApplicationServiceは上位レベルになります。
抽象型を利用しなかったとき、UserApplicationServiceは具体的な技術基盤と比べて上位レベルのモジュールでありながら、データストアの操作を行う下位レベルのモジュールであるUserRepositoryに依存していました。
これはまさに「上位レベルのモジュールは下位レベルのモジュールに依存してはならない」という原則に反しています。

## 抽象に依存する3
<style scoped>p {font-size: 84%; } </style>
![](./md_files/img/07/0706.PNG)
依存の関係はUserApplicationServiceが抽象型であるIUserRepositoryを参照するようになると図のように変化します。

抽象型を導入することでUserApplicationServiceとUserRepositoryは、双方ともに抽象型であるIUserRepositoryに依存の矢印を伸ばすことになります。
上位レベルのモジュール（UserApplicationService）が下位レベルのモジュール（UserRepository）に依存しなくなり、「どちらのモジュールも抽象に依存すべきである」という原則にも合致します。
もともと具体的な実装に依存していたものが抽象に依存するように、依存関係は逆転したのです。

## 抽象に依存する4

一般的に抽象型はそれを利用するクライアントが要求する定義です。
IUserRepositoryはいわばUserApplicationServiceのために存在しているといっても過言ではありません。
このIUserRepositoryという抽象に合わせてUserRepositoryの実装を行うことは、方針の主導権をUserApplicationServiceに握らせることと同義です。
「抽象は実装の詳細に依存してはならない。実装の詳細が抽象に依存すべきである」という原則はこのようにして守られます。

## 補足:主導権を抽象に

<style scoped>p {font-size: 82%; } </style>
伝統的なソフトウェア開発手法では高レベルなモジュールが低レベルなモジュールに依存する形で作成される傾向がありました。
言い換えるなら抽象が詳細に依存するような形で構築されていました。
抽象が詳細に依存するようになると、低レベルのモジュールにおける方針の変更が高レベルのモジュールに波及します。
これはおかしな話です。重要なドメインのルールが含まれるのはいつだって高レベルなモジュールです。
低レベルなモジュールの変更を理由にして、重要な高レベルのモジュールを変更する（たとえばデータストアの変更を理由にビジネスロジックを変更する）などということは起きてほしくない事態です。
主体となるべきは高レベルなモジュール、すなわち抽象です。
低レベルなモジュールが主体となるべきではありません。高レベルなモジュールは低レベルのモジュールを利用するクライアントです。
クライアントがすべきはどのような処理を呼び出すかの宣言です。
先述したとおり、インターフェースはそれを利用するクライアントが宣言するものであり、主導権はそのクライアントにあります。
インターフェースを宣言し、低レベルのモジュールはそのインターフェースに合わせて実装を行うことで、より重要な高次元の概念に主導権を握らせることが可能になるのです。

## 依存関係をコントロールする

UserApplicationServiceがインメモリで動作するテスト用のリポジトリを利用してほしいのか、
それともリレーショナルデータベースに接続するプロダクション用のリポジトリを利用してほしいのかどうかは、ときと場合によります。
開発中であれば前者でしょうし、リリースビルドはもちろん後者です。

それをどのようにして制御するかです。依存関係をコントロールする手段について確認していきます。
まずはあまりよくない例から見ていきましょう。

## 開発中にインメモリのリポジトリを利用するという目的を達成することだけを考えた短絡的なコード

```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;

  public UserApplicationService() {
    this.userRepository = new InMemoryUserRepository();
  }
  （…略…）
}
```

これでインメモリのリポジトリが使えるようになります
しかし、リリースの際にはプロダクション用リポジトリを利用するように完成したはずのコードを修正する必要があります。

## プロダクション用のリポジトリに差し替えるコード1

```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  public UserApplicationService() {
    //衝撃のコメントアウト
    // this.userRepository = new InMemoryUserRepository();
    this.userRepository = new UserRepository();
  }
  （…略…）
}
```

## プロダクション用のリポジトリに差し替えるコード2

もちろんこの作業はここだけにとどまりません。
この造りが許されるなら、きっと他のコードも似たような構造をしているでしょう。
それらも間違いなくプロダクション用リポジトリを取り扱うようにコードを修正していく必要があります。
修正自体は至極単純な作業と予測できますが、いかにも開発者向きでない愚直で面倒な作業です。

## 依存関係の問題
<style scoped>p {font-size: 86%; } </style>
リリースしたとしても、場合によってはインメモリのリポジトリを利用したくなるときがあります。
たとえばソフトウェアに不具合が発生したときの原因究明をしたいときなどがそれにあたります。
ソフトウェアに不具合が生じたとき、その不具合の状況を再現するためのデータをデータベースに用意するのは手間がかかります。
多くの場合「エラーを発生させるための整合性が取れたデータ」は用意しづらいものです。
こういったときはテスト用のリポジトリを用意して、それを利用するように差し替えて、プログラムの挙動を確かめたいところです。
そのとき開発者に与えられるのは、プロダクション用のリポジトリをまたテスト用のリポジトリに差し替える単調な作業です。
こういった問題を解決するために取られるパターンとして、Service LocatorパターンとIoC Containerパターンがあります。それぞれどういったものか確認していきましょう。

## Service Locatorパターン1
Service LocatorパターンはServiceLocatorと呼ばれるオブジェクトに依存解決先となるオブジェクトを事前に登録しておき、インスタンスが必要となる各所でServiceLocatorを経由してインスタンスを取得するパターンです。
以下のコードはテンプレートの特殊化を利用しています
[参考](https://qiita.com/hal1437/items/b6deb22a88c76eeaf90c#%E3%83%86%E3%83%B3%E3%83%97%E3%83%AC%E3%83%BC%E3%83%88%E3%81%AE%E7%89%B9%E6%AE%8A%E5%8C%96)

```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  public UserApplicationService() {
    // ServiceLocator経由でインスタンスを取得する
    this.userRepository = ServiceLocator.Resolve<IUserRepository>();
  }
  （…略…）
}
```


## Service Locatorパターン2

```
ServiceLocator.Register<IUserRepository, InMemoryUserRepository>();
```
コンストラクタでServiceLocatorにIUserRepositoryの依存を解決するように依頼しています。この依頼に対して返却される実際のインスタンスはスタートアップスクリプトなどで事前に登録しておきます
登録するとIUserRepositoryの依存解決が依頼された際にInMemoryUserRepositoryをインスタンス化して引き渡します。


## Service Locatorパターン3
```
// この修正のみで全体に変更が行き渡る
ServiceLocator.Register<IUserRepository, UserRepository>();
```

プロダクション用データベースに接続するリポジトリを利用したいときはServiceLocatorへの登録を上記のように変更することで対応します。
IUserRepositoryを要求するオブジェクトがすべてServiceLocator経由でインスタンスを取得していれば、修正箇所は依存関係を設定しているスタートアップスクリプトの修正だけでこと足ります。

## Service Locatorパターン4
![](./md_files/img/07/0707.PNG)
このようにServiceLocatorに依存を解決させることによりInMemoryUserRepositoryないしUserRepositoryのインスタンス化を行うコードがプログラムの随所に点在しなくなり、アプリケーションの中核を担うロジックに修正を加えることなく、リポジトリの実体を差し替えられるようになります

## Service Locatorパターン5

![](./md_files/img/07/0708.PNG)
ServiceLocatorに登録されるインスタンスの設定はプロダクション用とテスト用など用途に応じて一括で管理すると便利です。
スタートアップスクリプトでプロジェクトの構成設定などをキーにして、用途ごとのインスタンス設定に切り替えを行えるようにします

## Service Locatorパターン6

Service Locatorパターンは大がかりな仕掛けを用意する必要がないため導入しやすいものです。一方でService Locatorパターンはアンチパターンであるともいわれています。その理由は次の2つの問題を起因とします。

* 依存関係が外部から見えづらくなる
* テストの維持が難しくなる

## Service Locatorパターン依存関係が外部から見えづらくなる
```
public class UserApplicationService
{
  public UserApplicationService();//依存関係を表していない
  public void Register(UserRegisterCommand command);
}
```

Service Locatorパターンを採用した場合、コンストラクタはたいていひとつになります。これはService Locatorから必要なインスタンスを取り出すようになるためです。このとき外部から見えるクラス定義は上記のようになります。
UserApplicationServiceのコンストラクタは何も必要としていないように見えます。
実際にUserApplicationServiceをインスタンス化して使うには、事前にServiceLocatorにRepositoryを登録していないといけません。
引数以外に依存しています。これはほんとにやめてほしい

## テストの維持が難しくなる1

```
//memoryDBをセット
ServiceLocator.Register<IUserRepository,InMemoryUserRepository>();
var userApplicationService = new UserApplicationService();
```
例えば上記のようにテストを行うためのテストコードを書いていたとします
月日がたちuserApplicationServiceの依存関係が増えました

## テストの維持が難しくなる2

```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  // 新たなフィールドが追加された
  private readonly IFooRepository fooRepository;

  public UserApplicationService(){
    this.userRepository =ServiceLocator.Resolve<IUserRepository>();
    // ServiceLocator経由で取得している
    this.fooRepository =ServiceLocator.Resolve<IFooRepository>();
  }
  （…略…）
}
```
IFooRepositorの依存関係が増えました。
実際のコードも修正して動くようにしました。
しかしテストコードはテストを走らせてしかわかりません。

## 二つの問題の本質

二つの問題の本質は依存関係があるのに、それが外から見えないことです。
外というのは関数定義です。
関数定義からわからないってことはコンパイラにもわからないので、実行してからしかわからないということになります

## IoC Containerパターン

じゃあ結局どうすればいいの？
ってことで出てくるのが、IoCパターン(DI Container)です
IoCパターンの前にDIについて説明します

## Dependency Injectionパターン(DI)

Dependency Injection == 依存の注入
注入ってなんや！！ってなりますが、コード見たら納得できます

```
var userRepository = new InMemoryUserRepository();
var userApplicationService = new UserApplicationService(userRepository);
```

UserApplicationServiceにInMemoryUserRepositoryに対する依存を注
入、つまりDependency Injectionをしています。

## テストの維持が難しくなるを解決

```
public class UserApplicationService
{
  private readonly IUserRepository userRepository;
  // 新たにIFooRepositoryへの依存関係を追加する
  private readonly IFooRepository fooRepository;

  // コンストラクタで依存を注入できるようにする
  public UserApplicationService(IUserRepositoryuserRepository, IFooRepository fooRepository){
    this.userRepository = userRepository;
    this.fooRepository = fooRepository;
  }
  （…略…）
}
```

UserApplicationServiceでは新たな依存関係を追加するためコンストラクタに引数が追加されています。
これによりUserApplicationServiceをインスタンス化して実施しているテストはコンパイルエラーにより実行できなくなります

## IoC Containerパターン1
DIパターンでは解決できな問題があります。
それはUserApplicationServiceをインスタンス化する箇所が複数ある場合に、それを一括で変更できる方法がないということです。
そこでIoC Containerパターンを活用します。
IoC Containerパターンを簡単に説明すると以下です
DI + Service Locator = IoC Container

実際のコードはどうなるか見ていきましょう

## IoC Containerパターン2

```
// IoC Container
var serviceCollection = new ServiceCollection();
// 依存解決の設定を登録する
serviceCollection.AddTransient<IUserRepository,InMemoryUserRepository>();
serviceCollection.AddTransient<UserApplicationService>();
// インスタンスはIoC Container経由で取得する
var provider = serviceCollection.BuildServiceProvider();
var userApplicationService =provider.GetService<UserApplicationService>();
```

IoC Containerは設定にしたがって依存の解決を行い、インスタンスを生成します。
## IoC Containerパターン3

<style scoped>p {font-size: 88%; } </style>
処理の流れを追ってみましょう。オブジェクトのインスタンス化が始まるのは最終行からです。
GetService<UserApplicationService>が呼び出され、IoC ContainerはUserApplicationServiceを生成しようとします。
UserApplicationServiceはコンストラクタでIUserRepositoryを必要とするので、内部的に依存関係を解決し、IUserRepositoryを取得しようとします。
IUserRepositoryはInMemoryUserRepositoryを利用するように登録されているので、UserApplicationServiceはInMemoryUserRepositoryのインスタンスを受け取り、インスタンス化されます。

ここでこのパターンのうまいところはproviderを作って、それからインスタンスを生成することにより、処理が順番に依存していないところです。
何も考えなかったらserviceCollectionからそのままUserApplicationServiceを生成してしまうところです。
providerを作ることにより後から依存関係を変えて生成することができたり、順番に依存する処理が生まれてしまうことを防いでいます。

## まとめ

この章ではプログラムとは切っても切れない関係にある重要な概念の依存と、そのコントロールの仕方について学びました。
依存関係はソフトウェアを構築する上で自然と発生するものです。
しかしながらその取り扱い方を間違えると手の施しようがないほど硬直したソフトウェアを生み出すことに繋がります。
ソフトウェアは本来柔軟なものです。利用者を取り巻く環境の変化に対応し、利用者を助けるために柔軟に変化できるからこそ「ソフト」ウェアと呼ばれるのです。
依存を恐れる必要はありません。
依存すること自体は避けられなくとも、依存の方向性は開発者が絶対的にコントロールできるものです。
この章で学んだようにドメインを中心にして、主要なロジックを技術的な要素に依存させないように仕立て上げ、ソフトウェアの柔軟性を保つことを目指してください。